# toastand.tech web site

** Help wanted! **

Hosted with [Surge](https://surge.sh "web hosting for static web sites") at [toastand.tech](https://toastand.tech).

To deploy, from the project root, enter:

    surge .

and the site should upload. The file `.surgeignore` contains names of files and directories to omit from the surge upload, mainly `assets/` and `design/` since they contain large files.

## LICENSE

[MIT license](./LICENSE)

## Contributor Covenant

We are using the Contributor Covenant [Code of Conduct](./CodeOfConduct.md) on this project.

## Contributing

This is an open source project and organization. [Issues](https://gitlab.com/toastand.tech/tenants-of-toast-and-tech/issues/new) and [Requests](https://gitlab.com/toastand.tech/tenants-of-toast-and-tech/merge_requests/new) welcome.


## redesign Sun Mar 17 16:55:01 2019

I watched [Sarah Drasner's](https://twitter.com/sarah_edo "amazing woman, really") class ["Design for Developers"](https://frontendmasters.com/courses/design-for-developers/ "what an awesome course") on [Frontend Masters](https://frontendmasters.com/courses/design-for-developers/ "best technical training on the planet") yesterday, and needed to put what I just learned into practice and this website was the obvious choice.

### layouts

It occured to me the current site layout was pretty crap, and looked awful everywhere, not just on mobile.

I skethed out a couple layouts, and tossed these into Affinity Designer (because I can't afford Adobe):

![site layouts in SVG](./design/layouts.svg "Site layouts in SVG")

The end result, after "user testing" (i.e. me) was a bit different, but the practice was in taking the time to sketch them out on paper.

### colour

The colours of the previous design were chosen because I love the colour names. But this time, I wanted something that could invoke the "toasty" feel, so browns and yellows and oranges.

### images

I tried my hand at making a logo, but ![it pretty well sucks](./assets/happy-toast-2.svg "it pretty well sucks")
